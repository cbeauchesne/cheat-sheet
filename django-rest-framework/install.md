https://www.django-rest-framework.org/tutorial/quickstart/

```bash
virtualenv env
env\Scripts\activate # Windows..
pip install django
pip install djangorestframework
django-admin startproject test_project . # Note the trailing '.' character
cd test_project
django-admin startapp test_app
cd ..
python manage.py migrate
python manage.py createsuperuser --email admin@example.com --username admin

python manage.py runserver
curl -H "Accept: application/json; indent=4" -u admin:admin http://127.0.0.1:8000/users/
```
