
* [ ] [install](https://www.django-rest-framework.org/tutorial/quickstart/)
* [ ] [serialization](https://www.django-rest-framework.org/tutorial/1-serialization/)
* [ ] [requests-and-responses](https://www.django-rest-framework.org/tutorial/2-requests-and-responses/)
* [ ] [class-based-views](https://www.django-rest-framework.org/tutorial/3-class-based-views/)
* [ ] [authentication-and-permissions](https://www.django-rest-framework.org/tutorial/4-authentication-and-permissions/)
* [ ] [relationships-and-hyperlinked-apis](https://www.django-rest-framework.org/tutorial/5-relationships-and-hyperlinked-apis/)
* [ ] [viewsets-and-routers](https://www.django-rest-framework.org/tutorial/6-viewsets-and-routers/)
* [ ] [schemas-and-client-libraries](https://www.django-rest-framework.org/tutorial/7-schemas-and-client-libraries/)
